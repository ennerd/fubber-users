<?php
namespace Fubber\Users;

class Security {
    
    public static function session() {
        return \Nerd\Glue::get(\Fubber\Session\ISession::class);
    }
    
    public static function assertLoggedIn() {
        if(!self::session()->userId)
            throw new \Fubber\AccessDeniedException();
    }
    
}