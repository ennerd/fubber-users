<?php
namespace Fubber\Users;

class User extends \Fubber\Db\Table {
    public $id, $email, $password;
    
    public static $_table = 'f_users';
    public static $_cols = ['id','email','password'];
    
    public static function getCurrent(\Fubber\State $state) {
        session_start();
    }
}